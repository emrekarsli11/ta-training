package org.cb.ta;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.ArrayList;
import java.util.List;

public class HelloSelenium {
    public static void main(String[] args) throws InterruptedException {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        // ilk durum - given
        driver.get("https://www.aa.com/homePage.do");

        // aksiyon al - when
        WebElement oneWayRadioButtonDivElement = driver.findElement(
                By.xpath("//input[@type='radio'][@value='oneWay']/parent::div"));
        oneWayRadioButtonDivElement.click();

        // test - then
        WebElement roundTripRadioButtonElement = driver.findElement(
                By.xpath("//input[@type='radio'][@value='roundTrip']"));
        Assertions.assertFalse(roundTripRadioButtonElement.isSelected());

        WebElement oneWayRadioButtonElement = driver.findElement(
                By.xpath("//input[@type='radio'][@value='oneWay']"));
        Assertions.assertTrue(oneWayRadioButtonElement.isSelected());

        Thread.sleep(3000);
        driver.close();
    }
}
